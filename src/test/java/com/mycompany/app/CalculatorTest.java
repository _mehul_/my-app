package com.mycompany.app;



import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

//import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;




public class CalculatorTest {
	
	@Mock
	private Validator validator;
	
	private Calculator calc;
	
	public CalculatorTest() {
		MockitoAnnotations.initMocks(this);
		calc = new Calculator(validator);
	}
	
	

	@Test
	public void test_Add_PositiveNumbers_WhenValidatorTrue() {
		//Given
		int input1 = 1;
		int input2 = 4;
		when(validator.validate()).thenReturn(true);
		//When
		int out = calc.add(input1, input2);
		//Then
		assertEquals( 5, out);
	}
	
	@Test
	public void test_Add_PositiveNumbers_WhenValidatorFalse() {
		//Given
		int input1 = 1;
		int input2 = 4;
		when(validator.validate()).thenReturn(false);
		//When
		int out = calc.add(input1, input2);
		//Then
		assertEquals( 0, out);
	}
	
	@Test
	public void test_Add_PositiveNumbers_WhenValidatorUnknown() {
		//Given
		int input1 = 1;
		int input2 = 4;
		when(validator.validate()).thenThrow(new RuntimeException());
		//When
		int out = calc.add(input1, input2);
		//Then
		assertEquals( 0, out);
	}
	
	@Test
	public void test_Add_NegativeNumbers() {
		//Given
		int input1 = -1;
		int input2 = -7;
		when(validator.validate()).thenReturn(true);
		//When
		int out = calc.add(input1, input2);
		//Then
		assertEquals( -8, out);
	}
	
	@Test
	public void test_Divide_PositiveNumbers() {
		//Given
		int input1 = 9;
		int input2 = 3;
		when(validator.validate()).thenReturn(true);
		//When
		int out = calc.divide(input1, input2);
		//Then
		assertEquals( 3, out);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void test_Divide_DenumIsZero() {
		//Given
		int input1 = 6;
		int input2 = 0;
		when(validator.validate()).thenReturn(true);
		//When
		calc.divide(input1, input2);
		//Then
		//assertEquals( -8, out);
	}

}
