package com.mycompany.app;

public class Calculator {
	private Validator validator;
	
	public Calculator(Validator validator) {
		this.validator=validator;
	}

	public int add(int i, int j) {
		try {
			if(!validator.validate()) {
				return 0;
			}
			
		} catch (Exception e) {
			return 0;
		}
		return i + j;
		
	}

	public int divide(int input1, int input2) {
		if(input2 == 0) {
			throw new IllegalArgumentException();
		}
		return input1 / input2;
	}

}
