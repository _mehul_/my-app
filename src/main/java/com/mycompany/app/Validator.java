package com.mycompany.app;

public interface Validator {

	boolean validate();

}
